//
//  StudentsVC.swift
//  TheNorthwestProgrammingContest
//
//  Created by Student on 3/14/19.
//  Copyright © 2019 Student. All rights reserved.
//

import UIKit

class StudentsVC: UIViewController {
    
    @IBOutlet weak var student0LBL: UILabel!
    @IBOutlet weak var student1LBL: UILabel!
    @IBOutlet weak var student2LBL: UILabel!
    
    var studentTeam: Team!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        student0LBL.text = studentTeam.students[0]
        student1LBL.text = studentTeam.students[1]
        student2LBL.text = studentTeam.students[2]
        
        // Do any additional setup after loading the view.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
