//
//  NewTeamVC.swift
//  TheNorthwestProgrammingContest
//
//  Created by Student on 3/14/19.
//  Copyright © 2019 Student. All rights reserved.
//

import UIKit

class NewTeamVC: UIViewController {
    
    @IBOutlet weak var newTeamNameTF: UITextField!
    @IBOutlet weak var addNewStudent0TF: UITextField!
    @IBOutlet weak var addNewStudent1TF: UITextField!
    @IBOutlet weak var addNewStudent2TF: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    var addNewTeam :School!
    var newteamvc = Schools.shared
    
    @IBAction func cancelAddNewTeam(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func doneAddNewTeam(_ sender: Any) {
        let newTeamName = newTeamNameTF.text!
        let newStudent = addNewStudent0TF.text!
        let newStudent1 = addNewStudent1TF.text!
        let newStudent2 = addNewStudent2TF.text!
        if newTeamName == "" || newStudent == "" || newStudent1 == "" || newStudent2 == ""{
            
        }
            
        else{
            addNewTeam.addTeam(name: newTeamName, students: [newStudent, newStudent1, newStudent2])
            newteamvc.saveTeamForSelectedSchool(school: addNewTeam, team: Team(name: newTeamName, students:[newStudent,newStudent1,newStudent2]))
            self.dismiss(animated: true, completion: nil)
        }
    }
}
