//
//  NewSchoolVC.swift
//  TheNorthwestProgrammingContest
//
//  Created by Student on 3/14/19.
//  Copyright © 2019 Student. All rights reserved.
//

import UIKit

class NewSchoolVC: UIViewController {
    var newSchool: School!
    var newschoolvc = Schools.shared
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    @IBOutlet weak var addNewSchoolNameTF: UITextField!
    @IBOutlet weak var addNewCoachTF: UITextField!
    
    @IBAction func CancelAddNewSchool(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func doneAddNewSchool(_ sender: Any) {
        let schoolNew:String = addNewSchoolNameTF.text!
        let coachNew:String = addNewCoachTF.text!
        if schoolNew == "" || coachNew == "" {
            
        }
        else{
            newschoolvc.saveSchool(name: schoolNew, coach: coachNew)
            self.dismiss(animated: true, completion: nil)
        }
    }
    
}
