//
//  TeamsTVC.swift
//  TheNorthwestProgrammingContest
//
//  Created by Student on 3/14/19.
//  Copyright © 2019 Student. All rights reserved.
//

import UIKit

class TeamsTVC: UITableViewController {
    
    var team: School!
    var teamtvc: Schools!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        teamtvc = Schools.shared
        NotificationCenter.default.addObserver(self, selector: #selector(teamsForSelectedSchoolRetrieved),name: .TeamsForSelectedSchoolRetrieved, object:nil)
        
    }
    
    // MARK: - Table view data source
    @objc func teamsForSelectedSchoolRetrieved(){
        tableView.reloadData()
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        teamtvc.retrieveTeamsForSelectedSchoolAsynchronously()
        return team.teams.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let tableCell = tableView.dequeueReusableCell(withIdentifier: "totalteams", for: indexPath)
        tableCell.textLabel?.text = team.teams[indexPath.row].name
        
        return tableCell
    }
    
    override func viewWillAppear(_ animated: Bool) {
        tableView.reloadData()
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "teamTVC"{
            let studentsVC = segue.destination as! StudentsVC
            studentsVC.studentTeam = team.teams[tableView.indexPathForSelectedRow!.row]
        }
        else if segue.identifier == "addteam"{
            let newTeamsVC = segue.destination as! NewTeamVC
            newTeamsVC.addNewTeam = team
        }
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            team.teams.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
    }
}
