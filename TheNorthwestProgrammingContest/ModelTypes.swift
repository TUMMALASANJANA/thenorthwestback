//
//  ModelTypes.swift
//  TheNorthwestProgrammingContest
//
//  Created by Student on 3/14/19.
//  Copyright © 2019 Student. All rights reserved.
//

import Foundation

extension Notification.Name {
    static let SchoolsRetrieved = Notification.Name("Schools Retrieved")
    static let TeamsForSelectedSchoolRetrieved = Notification.Name("Teams for Selected School Retrieved")
    static let TeamsRetrieved = Notification.Name("Teams Retrieved")
}

@objcMembers
class Team:NSObject
{
    var name:String?
    var students:[String]
    var objectId:String?
    
    init(name:String,students:[String])
    {
        self.name = name
        self.students = students
    }
    
    convenience override init()
    {
        self.init(name:"",students:[])
    }
    
    static func == (lhs: Team, rhs: Team) -> Bool {
        return lhs.name == rhs.name && lhs.students == rhs.students
    }
}

@objcMembers
class School : NSObject
{
    var name:String!
    var coach:String!
    var teams:[Team]!
    var objectId:String?
    
    init(name:String,coach:String,teams:[Team])
    {
        self.name = name
        self.coach = coach
        self.teams = []
    }
    
    convenience override init()
    {
        self.init(name:"",coach:"",teams:[])
    }
    
    static func == (lhs: School, rhs: School) -> Bool {
        return lhs.name == rhs.name && lhs.coach == rhs.coach && lhs.teams == rhs.teams
    }
    
    func addTeam(name:String,students:[String])
    {
        
        teams.append(Team(name: name, students: students))
    }
}

class Schools
{
    let backendless = Backendless.sharedInstance()!
    var schoolDataStore:IDataStore!
    var teamsDataStore:IDataStore!
    static var shared:Schools = Schools()
    
    var schools:[School] = []
    var teams:[Team] = []
    
    convenience init() {
        self.init(schools:[])
    }
    
    var selectedSchool:School?
    var teamsForSelectedSchool:[Team] = []
 
    private init(schools:[School]){
        schoolDataStore = backendless.data.of(School.self)
        teamsDataStore = backendless.data.of(Team.self)
    }
    
    func numScools()->Int {
        return schools.count
    }
    
    func school(_ index:Int) -> School {
        return schools[index]
    }
    
    subscript(index:Int) -> School {
        return schools[index]
    }
    
    func add(school:School){
        schools.append(school)
    }
    
    func delete(school:School){
        for i in 0 ..< schools.count {
            if schools[i] == school {
                schools.remove(at:i)
                break
            }
        }
    }
    
    func retrieveAllTeams() {
        Types.tryblock({() -> Void in self.teams = self.teamsDataStore.find() as! [Team]}, catchblock: {(fault) -> Void in print(fault ?? "Something has gone wrong")})
    }

    func retrieveAllSchools() {
        let queryBuilder = DataQueryBuilder()
        queryBuilder!.setRelated(["teams"])
        queryBuilder!.setPageSize(100)
        Types.tryblock({() -> Void in
            self.schools = self.schoolDataStore.find(queryBuilder) as! [School]
        },
                       catchblock: {(fault) -> Void in print(fault ?? "Something has gone wrong")}
        )
    }
    
    func retrieveTeamsForSelectedSchoolAsynchronously() {
        let queryBuilder:DataQueryBuilder = DataQueryBuilder()
        queryBuilder.setWhereClause("name = '\(self.selectedSchool?.name ?? "")'" )
        queryBuilder.setPageSize(100)
        queryBuilder.setRelated( ["teams"] )
        self.schoolDataStore.find(queryBuilder,
                                  response: {(results) -> Void in
                                    let school = results![0] as! School
                                    self.teamsForSelectedSchool = school.teams
                                    NotificationCenter.default.post(name: .TeamsForSelectedSchoolRetrieved, object: nil)
        }, error: {(exception) -> Void in
        })
    }
    
    func saveSchool(name:String,coach:String)
    {
        var school = School(name: name, coach: coach, teams: [])
        school = schoolDataStore.save(school) as! School
        schools.append(school)
    }

    func deleteSchool(school:School){
        schoolDataStore.remove(school)
        for i in 0..<schools.count{
            if schools[i] == school{
                schools.remove(at: i)
            }
        }
    }
    
    func saveTeamForSelectedSchool(school:School, team:Team) {
        Types.tryblock({
            let team = self.teamsDataStore.save(team) as! Team
            self.schoolDataStore.addRelation("team:team:n", parentObjectId:   school.objectId, childObjects: [team.objectId!])
        }, catchblock:{ (exception) -> Void in
            print(exception.debugDescription)
        })
    }
    
}





